﻿using UnityEngine;
using System.Collections;

public class Move8Directions : MonoBehaviour {

    /*
        Movimento de 8 direções! 
        Para sprites virados para a direita, ainda não consegui fazer um jeito que funciona para sprites virados para cima.

        Por: Lucas Santos
    */

    public float speed;

    void Update()
    {
        //Chama função move se algum dos axis está sendo apertado. O GetAxisRaw só retorna valores 1, 0 ou -1.
        if(Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
        {
            move();
        }

    }

    void move()
    {
        //Rotaciona o objeto
        Vector3 direction = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0);
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        //Move para onde o objeto está "olhando"
        transform.Translate(Vector3.right * speed * Time.deltaTime);

    }
    

	
}
