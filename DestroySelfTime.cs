﻿using UnityEngine;
using System.Collections;

public class DestroySelfTime : MonoBehaviour {
    /*
    Destroi o objeto após tempo segundos. Simples, mas útil! :)

        Por : Lucas Santos.
    */


    public float tempo;
	void Start () {
        Destroy(gameObject, tempo);
	}
	
}
